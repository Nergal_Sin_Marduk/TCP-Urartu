#include "headers/server.h"
#include "headers/tcp_helper.h"
#include "headers/tcp_server_log.h"
#include <arpa/inet.h>
#include <asm-generic/socket.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>

int main(int argc, char **argv) {
  int sockfd, new_sockfd;
  int recv_length;
  struct sockaddr_in host_addr, client_addr;
  struct in_addr debug_addr;
  socklen_t sin_size;
  int setsockoptval = 1;
  int port = DEFAULT_SERVER_PORT;
  char host_ip[15];
  memset(host_ip, '\0', sizeof(host_ip));
  char buffer[1024];
  memset(buffer, '\0', sizeof(buffer));
  char *file_path = NULL;
  switch (argc) {
  case CLEAN_EXECUTION:
    usage(SERVER);
    break;
  case PORT_EXECUTION:
    port = atoi(argv[1]);
    if (port == 0) {
      printf("Unknown port, port will be %d by default\n", DEFAULT_SERVER_PORT);
    }
    break;
  case FULL_EXECUTION:
    port = atoi(argv[1]);
    if (port == 0) {
      printf("Unknown port, port will be %d by default\n", DEFAULT_SERVER_PORT);
    }
    memcpy(host_ip, argv[2], sizeof(host_ip));
    break;

  default:
    printf("Program must have not more than two parameters\n");
    break;
  }

  if ((sockfd = socket(PF_INET, SOCK_STREAM, 0)) == -1) {
    puts("SOCKET_ERROR");
    return SOCKET_ERROR;
  }

  if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &setsockoptval,
                 sizeof(int)) == -1) {
    puts("SETSOCKOPT_ERROR");
    return SETSOCKOPT_ERROR;
  }

  inet_aton(host_ip, &debug_addr);
  host_addr.sin_family = AF_INET;
  host_addr.sin_port = htons(port);
  host_addr.sin_addr.s_addr = *host_ip == '\0' ? INADDR_ANY : debug_addr.s_addr;
  printf("[DEBUG] IP address is %s and transform to network is %u\n", host_ip,
         debug_addr.s_addr);
  memset(host_addr.sin_zero, '\0', 8);

  if (bind(sockfd, (struct sockaddr *)&host_addr, sizeof(struct sockaddr)) ==
      -1) {
    puts("BIND_ERROR");
    return BIND_ERROR;
  }

  if (listen(sockfd, 5) == -1) {
    puts("LISTEN_ERROR");
    return LISTEN_ERROR;
  }

  while (666) {
    sin_size = sizeof(struct sockaddr_in);
    new_sockfd = accept(sockfd, (struct sockaddr *)&client_addr, &sin_size);
    if (new_sockfd == -1) {
      puts("ACCEPT_ERROR");
      return ACCEPT_ERROR;
    }
    send(new_sockfd, "Hello, world\n", 13, 0);
    recv_length = recv(new_sockfd, buffer, sizeof(buffer), 0);
    while (recv_length > 0) {
      printf("buffer: %s\n", buffer);
      file_path = find_file_pattern(buffer, sizeof(buffer));
      if (file_path) {
        send_file(file_path, new_sockfd);
        file_path = NULL;
      }
      printf("[DEBUG] FilePath: %s\n", file_path);
      memset(buffer, '\0', sizeof(buffer));
      recv_length = recv(new_sockfd, buffer, sizeof(buffer), 0);
    }
    close(new_sockfd);
  }

  return 0;
}

char *find_file_pattern(const char *buffer, unsigned int length) {
  char template_str[11] = "$ GET FILE:";
  int i, j, z = 0;
  int match = 0;
  char *ptr = NULL;

  for (i = 0; i < length; i++) {
    if (buffer[i] == *template_str) {
      for (j = i; j < i + sizeof(template_str) - 1; j++, z++) {
        if (buffer[j] != template_str[z]) {
          break;
        }
        match++;
      }
      if (match >=
          sizeof(template_str) - 2) { // 2 so user had right on error in the end
                                      // of command it can changes to taste
        ptr = &buffer[++j];
        return ptr;
      }
      z = match = 0;
    }
  }
  return ptr;
}
/*
 *Send bytes without size limit
 *path to file count after : in command {$ GET FILE:[this]}
 *
 * */
int send_file(const char *path, const int sock) {
  int fd, i, file_length = 0, j;
  unsigned char byte;
  unsigned char send_buffer[1024];
  memset(send_buffer, '\0', sizeof(send_buffer));
  char transform_path[256]; // so we should a clean name without \r\n in the end
                            // of path and this is cut that
  memset(transform_path, '\0', sizeof(transform_path));
  for (i = 0; i < sizeof(transform_path); i++) {
    if (*(path + i) == 0x0D) { // 0x0D means \r in hex view
      break;
    }
    transform_path[i] = *(path + i);
  }

  printf("[DEBUG] path to file is %s", transform_path);
  fd = open(transform_path, O_RDONLY);
  if (fd == -1) {
    send(sock, "Error 404\n", strlen("Error 404\n"), 0);
    close(fd);
    return -1;
  } else {
    send(sock, "File found, lets start a transfer\n",
         strlen("File found, lets start a transfer\n"), 0);
    while ((j = read(fd, send_buffer, sizeof(send_buffer)) > 0)) {
      send(sock, send_buffer, sizeof(send_buffer), 0);
      memset(send_buffer, '\0', sizeof(send_buffer));
    }
    send(sock, send_buffer, sizeof(send_buffer), 0);
  }
  close(fd);
  return 0;
}
