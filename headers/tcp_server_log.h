#ifndef _TCP_SERVER_LOG_H_

#define _TCP_SERVER_LOG_H_

typedef enum {
  SOCKET_ERROR = -1,
  SETSOCKOPT_ERROR = -2,
  BIND_ERROR = -3,
  LISTEN_ERROR = -4,
  ACCEPT_ERROR = -5
} Enum_Log_Connection_Error;

typedef enum {
  CLEAN_EXECUTION = 1,
  PORT_EXECUTION = 2,
  FULL_EXECUTION = 3
} Enum_Log_User_Parameters;

#endif
