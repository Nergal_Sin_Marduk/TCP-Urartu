#ifndef _TCP_HELPER_H_
#define _TCP_HELPER_H_

#define DEFAULT_SERVER_PORT 6666

typedef enum { SERVER, CLIENT } Enum_Program_Number;

int usage(int program_number);

#endif
