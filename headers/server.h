#ifndef _SERVER_H_
#define _SERVER_H_

char *find_file_pattern(const char *buffer, unsigned int length);
int send_file(const char *path, const int sock);

#endif
