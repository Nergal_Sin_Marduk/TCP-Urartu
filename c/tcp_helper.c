#include "../headers/tcp_helper.h"
#include <stdio.h>

int usage(int program_number) {
  switch (program_number) {
  case SERVER:
    printf("usage: ./server {port} {ip_address}\n");
    break;
  case CLIENT:
    printf("usage: ./client\n");
    break;
  default:
    printf("usage interrupted: unknown program_number\n");
    break;
  }
  return 0;
}
